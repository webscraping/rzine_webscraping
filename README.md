# Titre fiche [<img src="https://rzine.fr/img/Rzine_logo.png"  align="right" width="120"/>](http://rzine.fr/)
### Sous-titre fiche
**Auteur.e.s (affiliation.s)**
<br/>  

Résumé de la fiche....


Pour la consulter, cliquez [**ici**](https://webscraping.gitpages.huma-num.fr/rzine_webscraping)

<br/>  

[![DOI:10.48645/xxxx-xxxx](https://zenodo.org/badge/DOI/10.48645/xxxx-xxxx.svg)](https://doi.org/10.48645/xxxx-xxxx)
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-sa/4.0/)


- Lien vers slides intro : https://slides.com/sebastienreycoyrehourcq/element-r-webscraping-2024  
- Lien court vers site deployé : https://page.hn/4zrbg3